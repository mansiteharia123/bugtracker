import HttpStatus from 'http-status-codes'
// import bcrypt from 'bcrypt';

const FILLALLFIELDS = "Please Provide All Details.";
const NO_DATA_FOUND = "No Data found.";
const UNAUTHORIZED = "Sorry, you are not authorised to access this url."
const SALT_WORK_FACTOR = 10;
const DOOR = 'door';
const PARTNER = 'partner';
const YES = 'yes';
const NO = 'no';
// const BASE_PATH = "https://doorbackend.herokuapp.com";
const BASE_PATH = "http://180.151.103.85:4000";
const EXCEL_BASE_PATH = BASE_PATH + '/excel/';
const FRONTEND_BASE_PATH = "http://site4demo.com/door_testing/dist/#/";
const RESET_PASSWORD_PATH = FRONTEND_BASE_PATH + "reset-password/";

const DATA = {
    "Add": " added successfully.",
    "FOUND": "Data found successfully.",
    "NOT_FOUND": "No Data found.",
    "SAVE": "Data save successfully.",
    "DELETE": "Data deleted successfully.",
    "UPDATE": "Data updated successfully.",
    "NO_FILE": "No File Uploaded.",
    "INVALID_ENUM": "This is not a valid enum.",
    "STATUS_REQUIRED": "Status is required.",
    "DOOR_STATUS_REQUIRED": "Door Status is required."
}
const REPORT = {
    "ALREADY_APPROVED": "Report is already approved."
}
const JWT = {
    "SECRET_KEY": "door_abhishek",
    "EXPIRY_TIME": Date.now() + (60 * 60 * 1000),
}

const LOGIN = {
    "REQUIRED": "Please Provide All Details.",
    "SUCCESS": "Login successful.",
    "INVALID_EMAIL": "Enter valid email.",
    "INVALID_PASSWORD": "Enter valid password.",
    "DEACTIVATED": "Your account is currently deactivated. Please contact to DOOR."
}

const LOGOUT = {
    "SUCCESS": "Logout successful.",
    "NOT_FOUND": "Already logout."
}

const PASSWORD = {
    "REQUIRED": "Please provide all valid details.",
    "INVALID_EMAIL": "Enter valid email.",
    "SUCCESS": "Password has been successfully updated.",
    "INCORRECT": "Incorrect old password.",
    "MISMATCHED": "Confirm password mismatched.",
    "SAME": "Old password & new password can't be same.",
    "SENT_EMAIL": "Email sent successfully.",
    "EXPIRED": "Invalid URL.",
    "RESET_PASSWORD_EXPIRY": Date.now() + 3600000
}

const GMAIL_SMTP_CREDENTIAL = {
    "TYPE": "SMTP",
    "SERVICE": "Gmail",
    "HOST": "smtp.gmail.com",
    "PORT": 587,
    "SECURE": true,
    "USERNAME": "door.wildnet@gmail.com",
    "PASSWORD": "wildnet@123",
}

const PATTERN = {
    "PASSWORD": /(?=.*[A-Za-z])[a-zA-Z\d$@$!%*#?&]{6,10}/
}

const MAIL = {
    "ADD_PARTNER": "Welcome to Bug Tracker",
    "CONFIRMATION_MAIL": "Confirmation Mail",
    "RESET_PASSWORD": "Reset Password"
}

const ROYALTY_FEE = {

    "ALREADY_ACTIVE": "This period is already active.",
    "DOOR_STATUS_ONLY": "Please update door status only.",
    "STATUS_ONLY": "Please update status only.",
    "INVALID_ENUM": "This is not a valid enum.",
    "NO_ROYALTY": "No royalty is added, please contact to door or select some other period.",
    "CANNOT_UPDATE_DATE": "Start or End Date can't be update.",
    "DUPLICATE": "Duplicate royalty name not allowed.",
    "INVALID_DATE": "Start Date can't be less than End Date.",
    "INVALID_ID": "Invalid id."
}

const twilioCredentials = {
    "TwilioNumber": "+19149337525",
    "Authy": "BT3ybwzIHaDiNghYsUCbnajVUk93AxUf",
    "ACCOUNTSID": "ACb76e2a9503584eee8836854bc8bb40eb",
    "AUTHTOKEN": "1e41430304caa28891a9577f0954eaff"

};

const monthName = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

const setExcelStyle = (fontColor, fontSize, isBold, bgColor, align = "center", numFormat) => {
    return {
        font: {
            color: {
                rgb: fontColor
            },
            sz: fontSize,
            bold: isBold,
            underline: false
        },
        numFmt: numFormat,
        fill: {
            fgColor: {
                rgb: bgColor
                // rgb: '0071b342'
            }
        },
        alignment: {
            wrapText: true,
            horizontal: align,
            vertical: "center"
        },
        border: {
            top: { style: "thin", color: "FFFF0000" },
            bottom: { style: "thin", color: "FFFF0000" },
            left: { style: "thin", color: "FFFF0000" },
            right: { style: "thin", color: "FFFF0000" }
        }
    }
}

const getSaveMessage = (data, model) => {
    if (model) {
        return { message: model + DATA.Add, status: HttpStatus.OK }
    }
    else {
        return { message: DATA.SAVE, status: HttpStatus.OK }
    }
}

const getDataMessage = data => {
    if (data.length)
        return { message: DATA.FOUND, data: data, status: HttpStatus.OK }
    else
        return { message: DATA.NOT_FOUND, data: data, status: HttpStatus.OK }
}

const getUpdateMessage = data => {
    if (data.n)
        return { message: DATA.UPDATE, status: HttpStatus.OK }
    else
        return { message: DATA.NOT_FOUND, status: HttpStatus.NOT_FOUND }
}

const getDeleteMessage = data => {
    if (data.result.n)
        return { message: DATA.DELETE, status: HttpStatus.OK }
    else
        return { message: DATA.NOT_FOUND, status: HttpStatus.NOT_FOUND }
}

const getLoginMessage = (data, token) => {
    if (data) {
        let { _id, email, accountType, avatar, belongsTo, name, country, royaltyFeeStatus } = data._doc,
            resData = { _id, email, name, accountType, avatar, token, belongsTo, country, royaltyFeeStatus };
        return { message: LOGIN.SUCCESS, data: resData, status: HttpStatus.OK }
    }
    else
        return { message: DATA.NOT_FOUND, status: HttpStatus.NOT_FOUND }
}

const getLoginErrorMessage = response => {
    if (response && !response.status)
        return { message: LOGIN.DEACTIVATED, status: HttpStatus.NOT_FOUND }
    else
        return { message: LOGIN.INVALID_EMAIL, status: HttpStatus.NOT_FOUND }
}

var constants = {
    BASE_PATH: BASE_PATH,
    EXCEL_BASE_PATH: EXCEL_BASE_PATH,
    FRONTEND_BASE_PATH: FRONTEND_BASE_PATH,
    RESET_PASSWORD_PATH: RESET_PASSWORD_PATH,
    SALT_WORK_FACTOR: SALT_WORK_FACTOR,
    FILLALLFIELDS: FILLALLFIELDS,
    DATA: DATA,
    REPORT: REPORT,
    NO_DATA_FOUND: NO_DATA_FOUND,
    UNAUTHORIZED: UNAUTHORIZED,
    JWT: JWT,
    LOGIN: LOGIN,
    LOGOUT: LOGOUT,
    PASSWORD: PASSWORD,
    GMAIL_SMTP_CREDENTIAL: GMAIL_SMTP_CREDENTIAL,
    PATTERN: PATTERN,
    MAIL: MAIL,
    DOOR: DOOR,
    PARTNER: PARTNER,
    ROYALTY_FEE: ROYALTY_FEE,
    YES: YES,
    NO: NO,
    monthName: monthName,
    setExcelStyle: setExcelStyle,
    getSaveMessage: getSaveMessage,
    getDataMessage: getDataMessage,
    getUpdateMessage: getUpdateMessage,
    getDeleteMessage: getDeleteMessage,
    getLoginMessage: getLoginMessage,
    twilioCredentials: twilioCredentials,
    getLoginErrorMessage: getLoginErrorMessage
};

module.exports = constants;


/* 
const twilioCredentials = {
    "TwilioNumber" : "+19149337525",
    "Authy" : "BT3ybwzIHaDiNghYsUCbnajVUk93AxUf",
    "ACCOUNTSID"   : "ACb76e2a9503584eee8836854bc8bb40eb",
    "AUTHTOKEN"    : "1e41430304caa28891a9577f0954eaff"

};

const gmailSMTPCredentials = {
    "type": "SMTP",
    "service": "Gmail",
    "host": "smtp.gmail.com",
    "port": 587,
    "secure": false,
    "username": "mvsingh7595@gmail.com",
    "password": "mvsingh@123"
};

const smsCredentials = {
    number:'4755292423'
};

const rpiCredentials = {
    baseUrl:'http://proxy7.remote-iot.com:11804'
};

const pagination = {
    itemPerPage:10
};

const imagePaths = {
    "user": "/../../public/images/user/avatar",
    "url": "/images/user/avatar",
    "defaultUserImage" : './images/user/avtar.png'
};


const userRole = {
    "roles" : [
    {roleId : 1, roleName : "Admin"},
    {roleId : 2, roleName : "Developer"},
    {roleId : 3, roleName : "Tester"},
    {roleId : 4, roleName : "Business Developer"},
    {roleId : 5, roleName : "Project Manager"},
    {roleId : 6, roleName : "Team Manager"},
    {roleId : 7, roleName : "Team Lead"},
    {roleId : 8, roleName : "Super Admin"},
    {roleId : 9, roleName : "Desinger"}]
}

const hostingServer ={
    serverName : 'http://localhost:3000/',
    serverUiName : 'http://localhost:3001/',
    // serverName : 'https://bug-tracker-web.herokuapp.com'
}

var obj = { gmailSMTPCredentials:gmailSMTPCredentials, 
    twilioCredentials: twilioCredentials,
    smsCredentials:smsCredentials,
    imagePaths: imagePaths,
    rpiCredentials:rpiCredentials,
    pagination: pagination,
    // pagination: pagination,
    hostingServer: hostingServer,
    userRole : userRole};

module.exports = obj; */